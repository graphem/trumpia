<?php

/*
 * This file is part of the Trumpia library.
 *
 * (c) Guillaume Bourdages <gbourdages@graphem.ca>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Trumpia\Exception;

/**
 * @author Guillaume Bourdages <gbourdages@graphem.ca>
 */
interface ExceptionInterface
{
    //
}
