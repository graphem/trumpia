<?php

/*
 * This file is part of the Trumpia library.
 *
 * (c) Guillaume Bourdages <gbourdages@graphem.ca>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Trumpia;

/**
 * @author Guillaume Bourdages <gbourdages@graphem.ca>
 */

use Trumpia\Adapter\GuzzleHttpAdapter;
use Trumpia\Api\CustomData;
use Trumpia\Api\EmailSenderAddress;
use Trumpia\Api\Keyword;
use Trumpia\Api\Message;
use Trumpia\Api\Publication;
use Trumpia\Api\Report;
use Trumpia\Api\ResourceBalance;
use Trumpia\Api\SNSAccount;
use Trumpia\Api\Subscription;
use Trumpia\Api\TList;
use Trumpia\Api\Orgname;

class Trumpia
{
    
    /**
     * @var string
     */
    const ENDPOINT = 'http://api.trumpia.com/rest/v1/';

    /**
     * @var string
     */
    protected $endpoint;
    /**
     * @var String
     */
    protected $username;

    /**
     * @var AdapterInterface
     */
    protected $apiKey;

    /**
     * @var String
     */
    public $report;

    /**
     * @param AdapterInterface $adapter
     */
    public function __construct($username, $apiKey, $endpoint = null)
    {  
        $this->username = $username;
        $this->apiKey = $apiKey;
        $this->endpoint = $endpoint ?: static::ENDPOINT;
        $this->endpoint = $this->endpoint.$username;
        
        $this->setAdapter();
    }

    /**
     * Initiate the client for API transation
     *
     * @param  
     * @return 
     */
    public function setAdapter(AdapterInterface $adapter = null)
    {
        if(is_null($adapter)){
            $this->client = new GuzzleHttpAdapter($this->apiKey,$this->endpoint);
            return $this;
        }
        $this->client = $adapter($this->apiKey,$this->endpoint);
        return $this;        
    }

    /**
     * Get the client
     *
     * @param  
     * @return 
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * @return Custom Data
     */
    public function customData()
    {
        return new CustomData($this);
    }

    /**
     * @return Email Sende rAddress
     */
    public function emailSenderAddress()
    {
        return new EmailSenderAddress($this);
    }

    /**
     * @return Keyword
     */
    public function keyword()
    {
        return new Keyword($this);
    }    

    /**
     * @return Message
     */
    public function message()
    {
        return new Message($this);
    }

    /**
     * @return Publication
     */
    public function publication()
    {
        return new Publication($this);
    }

    /**
     * @return Report
     */
    public function report()
    {
        return new Report($this);
    }

    /**
     * @return Resource Balance
     */
    public function resourceBalance()
    {
        return new ResourceBalance($this);
    }

    /**
     * @return SNS Account
     */
    public function snsAccount()
    {
        return new SNSAccount($this);
    }

    /**
     * @return Subscription
     */
    public function subscription()
    {
        return new Subscription($this);
    }

    /**
     * @return List
     */
    public function tList()
    {
        return new TList($this);
    }

    /**
    * @return Orgnames
    */
    public function orgname()
    {
        return new Orgname($this);
    }
}