<?php

/*
 * This file is part of the Trumpia library.
 *
 * (c) Guillaume Bourdages <gbourdages@graphem.ca>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Trumpia\Helper;

/**
 * @author Guillaume Bourdages <gbourdages@graphem.ca>
 */

/**
* Report Class
*/
class Report{

    /**
     * Get error message
     *
     * @param  id of the request
     * @return string error message
     */

    public static function getErrorMessage($data = array())        
    {
        
        if(!property_exists($data[0], 'error_message')){
            return null;
        }

        return $data[0]->error_message;
    }

    /**
     * Get status code
     *
     * @param  id of the request
     * @return string status code
     */

    public static function getStatusCode($data )       
    {
        
        if(!property_exists($data[0], 'status_code')){
            return null;
        }

        return $data[0]->status_code;

    }

    /**
     * Check if already registered tool error
     *
     * @param  id of the request
     * @return string status code
     */

    public static function isDuplicate($data = array())       
    {

        if (strpos(self::getStatusCode($data), 'MPSE2401') !== false) {
            return true;
            exit;
        }

        return false;
        
    }

    /**
      * Get duplicate fields name and value
      */ 

    public static function getDuplicate($data) {

        $duplicates = [];

        $report_array = get_object_vars($data[0]);


        foreach ($report_array['error_data'] as $error) {
            
            $key = array_search($error, $report_array);

            if ($key) {
                
                $duplicates[$key] = $error;
            
            } elseif ($report_array['mobile']->number == $error) {
                
                $duplicates['mobile'] = $error;
            }
            
        }

        return $duplicates;

    }


} 