<?php

/*
 * This file is part of the Trumpia library.
 *
 * (c) Guillaume Bourdages <gbourdages@graphem.ca>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Trumpia\Api;

/**
 * @author Guillaume Bourdages <gbourdages@graphem.ca>
 */
use Trumpia\Trumpia;

abstract class AbstractApi
{
    
    /**
     * @var string
     */
    protected $service;

    /**
     * @var string
     */
    protected $return;

    /**
     * @var string
     */
    protected $api;

    /**
     * @var string
     */
    protected $client;

    /**
     * Request data when doing create or update method
     *
     * @var string
     */

    protected $requestData;

    protected $fields = array();
    

    public function __construct(Trumpia $client)
    {
        $this->client = $client;
        $this->api = $client->getClient();
    }

    /**
     * Get by id
     *
     * @param  id of the list
     * @return array list
     */

    public function fetch($id)        
    {
        $this->return = $this->api->get('/' . $this->service . '/'.$id);

        return $this->getResults();
    }

    /**
     * Fetch all
     *
     * @param  null
     * @return array of results
     */

    public function fetchAll($page = '', $rowSize = '')        
    {
        $this->return = $this->api->get('/' . $this->service);

        return $this->getResults();
    }

    /**
     * Create a record on a service
     *
     * @param  null
     * @return result
     */

    public function create()
    {
        $this->setRequestData();
        
        $this->return = $this->api->put('/' . $this->service, $this->getRequestData());

        return $this->getResults();
    }

    /**
     * Update a record
     *
     * @param  string $id
     * @return result
     */

    
    public function update($id)
    {
        $this->setRequestData();
    
        $this->return = $this->api->post('/' . $this->service . '/'.$id, $this->getRequestData());

        return $this->getResults();
    }

    /**
     * Delete a record
     *
     * @param  string id of record
     * @return result
     */

    public function delete($id)
    {      
        $this->return = $this->api->delete('/' . $this->service . '/'.$id);

        return $this->getResults();
    }

    /**
     * Get the result in array/object from json
     *
     * @param  string id of record
     * @return result
     */

    public function getReport(){

        $result = $this->getResults();

        $report = $this->client->report();

        return $report->fetch($result->request_id);
    }

    /**
     * Get the result in array/object from json
     *
     * @param  string id of record
     * @return result
     */

    public function getResults(){
        // Todo Log error
        return json_decode($this->return);
    }

    /**
     * Gets the value of requestData.
     *
     * @return mixed
     */
    public function getRequestData()
    {
        return $this->requestData;
    }

    /**
     * Set the value of the requet data
     *
     * @return mixed
     */

    protected function setRequestData($data = ''){
        
        $this->requestData = $data;

        return $this;
    }

    public function set($name, $value = null)
    {
        if (is_array($name)) {
            
            foreach ($name as $index => $val) {
                $this->fields[$index] = $val;                
            }

            return $this;
        }

        $this->fields[$name] = $value;

        return $this;
    }

    public function get($name)
    {
        return $this->fields[$name];
    }

    /**
     * Get error message clearly
     *
     * @param  
     * @return 
     */
    public function getErrorMessage($code)
    {
        if(array_key_exists ( $code , $this->errorMessage )){
            return $this->errorMessage[$code];
        }
        return 'Error '.$code;
    }
}