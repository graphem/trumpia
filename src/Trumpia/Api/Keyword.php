<?php

/*
 * This file is part of the Trumpia library.
 *
 * (c) Guillaume Bourdages <gbourdages@graphem.ca>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Trumpia\Api;

/**
 * @author Guillaume Bourdages <gbourdages@graphem.ca>
 */

use Trumpia\Helper\Helper;
use Trumpia\Helper\Report as ReportHelper;


class Keyword extends AbstractApi
{

    /**
     * Service of the class
     *
     * @var string
     */

    protected $service = "keyword";

    protected $fields = array(
        'keyword'    => '',
        'lists' => '',
        'allowMessage'   => '',
        'autoResponse' => '',
        'notify' => ''
    );


    protected function setRequestData($data = '')
    {

        $this->requestData = array(
            "keyword"    => $this->fields['keyword'],
            "lists" => $this->fields['lists'],
            "allow_message"    => $this->fields['allowMessage'],
            "auto_response"  => $this->fields['autoResponse'],
            "notify" => $this->fields['notify']
        );

        $this->requestData = Helper::cleanArray($this->requestData);

        return $this;
    }

}
