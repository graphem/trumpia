<?php

/*
 * This file is part of the Trumpia library.
 *
 * (c) Guillaume Bourdages <gbourdages@graphem.ca>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Trumpia\Api;

/**
 * @author Guillaume Bourdages <gbourdages@graphem.ca>
 */

use Trumpia\Helper\Helper;
use Trumpia\Helper\Report as ReportHelper;

class Subscription extends AbstractApi
{

    protected static $searchType = array(
        '1'   => 'EMAIL',
        '2'   => 'MOBILE',
        '3'   => 'LANDLINE',
        '4'   => 'AIM',
        '102' => 'COUNTRYCODE',
        '103' => 'CREATEDDATE',
        '104' => 'FIRSTNAME',
        '105' => 'LASTNAME',
    );

    protected $fields = array(
        'listName'            => '',
        'firstName'           => '',
        'lastName'            => '',
        'email'               => '',
        'aim'                 => '',
        'mobileNumber'        => '',
        'mobileCountryCode'   => '',
        'landlineNumber'      => '',
        'landlineCountryCode' => '',
        'voiceDevice'         => '',
        'customData'          => '',
    );

    protected $errorMessage = array(
        'MPSE0401'    => 'You cannot add a subscription without submitting an API contract.',
        'MPSE0501'    => 'The contact data being added has been blocked. The contact may have previously opted out of the system. Note: Contact data include email, mobile numbers and landline numbers.',
        'MPSE1001'    => 'The allow message parameter only accepts LANDLINE or MOBILE as values.',
        'MPSE1002'    => 'Valid values are fixed by the options of the custom data field. You can get the valid values via GET Custom Data Field.',
        'MPSE1101'    => 'Each list name can only be 1-30 characters in length.',
        'MPSE1102'    => 'The first name of the subscription exceeds the maximum length allowed.',
        'MPSE1103'    => 'The last name of the subscription exceeds the maximum length allowed.',
        'MPSE1105'    => 'Max length is fixed by the options of the custom data field. You can get the valid values via GET Custom Data Field.',
        'MPSE1106'    => 'Mobile number must be 10 characters when the country_code is 1, and 1-20 characters when the country_code is not 1.',
        'MPSE1107'    => 'Landline number must be 10 characters.',
        'MPSE1201'    => 'The list name includes special characters that are not allowed.',
        'MPSE1204'    => 'The custom data ID can only include numerical values; no other characters are allowed.',
        'MPSE1205'    => 'The custom data value includes special characters that are not allowed.',
        'MPSE1208'    => 'The mobile number is only made up of numerical values; no other characters are allowed.',
        'MPSE1209'    => 'The landline number is only made up of numerical values; no other characters are allowed.',
        'MPSE2001'    => 'The phone number was invalid for voice messaging. Only the US, Canada, Guam, and Puerto Rico are supported for voice messaging.',
        'MPSE2004'    => 'Country_code should be 1 only for landline number. Only US landline number is supported.',
        'MPSE2101'    => 'The list name is empty.',
        'MPSE2102'    => 'The subscriptions are empty.',
        'MPSE2103'    => 'Tool information is missing for the subscription being added.',
        'MPSE2109'    => 'A custom data ID has not been entered.',
        'MPSE2110'    => 'A value has not been entered.',
        'MPSE2201'    => 'The mobile number is incorrectly formatted.',
        'MPSE2202'    => 'The landline number is incorrectly formatted.',
        'MPSE2203'    => 'The email address is incorrectly formatted.',
        'MPSE2207'    => 'The format of the date\'s value is incorrect. The valid format is either YYYY-MM-DD or YYYY-MM-DD hh:mm:ss. You can check the valid formatting via PUSH Notification.',
        'MPSE2208'    => 'Mobile number may not start with 0 or 1 when the country_code is 1, and may not start with 0 when the country_code is not 1.',
        'MPSE2209'    => 'Landline number may not start with 0 or 1.',
        'MPSE2302'    => 'The list name being added was not found.',
        'MPSE2306'    => 'The custom data ID you entered does not exist.',
        'MPSE2401'    => 'The contact data being added has already been registered. Note: Contact data include email, mobile number, and landline number.',
        'MPSE2501'    => 'The value must be within the range specified in the custom data field. You can get the number range via GET Custom Data Field.',
        'MPSE2502'    => 'Valid value is decided by the default and interval values. You can get these values via GET Custom Data Field.',
        'MPSE2503'    => 'The carrier for this mobile number does not support Free to End User. Supported carriers: AT&T, Sprint, T-Mobile, Verizon Wireless, MetroPCS (GSM), Boost, Virgin Mobile.'
    );

    protected $service = 'subscription';

    /**
     * Get all subscription
     *
     * @param
     * @return
     */

    public function fetchAll($page = '', $rowSize = '')
    {
        $query = array(
            'query' => array(
                'row_size' => $rowSize,
                'page'     => $page,
            ),
        );
        $this->return = $this->api->get('/subscription', $query);

        return $this->getResults();
    }    

    protected function checkDuplicate() {

        $result = array();
        // check if email duplicate
        $subscription_id = '';
        $subscription = $this->search("email", $this->fields['email']);

        if(isset($subscription->subscription_id_list)){
            $subscription_id = $subscription->subscription_id_list[0];
        }

        if (!empty($subscription_id)) {
            
            $result = array(
                "status" => "101",
                "description" => "email duplicate",
                "email" => $this->fields['email'],
                "subscription id" => $subscription_id
            );

            return $result;
 
        }

        $subscription = $this->search("mobile", $this->fields['mobileNumber']);

        if(isset($subscription->subscription_id_list)){
            $subscription_id = $subscription->subscription_id_list[0];
        }

        if (!empty($subscription_id)) {
            
            $result = array(
                "status" => "102",
                "description" => "mobile duplicate",
                "mobile" => $this->fields['mobileNumber'],
                "subscription id" => $subscription_id
            );

            return $result;
 
        }

        $result = array(
            "status" => "100",
            "description" => "No Duplicate"
        );

        return $result;
    }

    /**
     * Search method
     *
     * @param  String searchType, String searchData, Sting listId
     * @return result
     */

    public function search($by, $searchData, $listId = '')
    {
        $by = strtoupper($by); 

        $fieldCode = array_search($by, self::$searchType);
        $query = array(
            'query' => 'search_type=' . $fieldCode . '&search_data=' . $searchData,
        );

        !empty($listId) ? $query['query'] = $query['query'] . '&list_id=' . $listId : $query['query'];

        $this->return = $this->api->get('/subscription/search', $query);
        
        return $this->getResults();
    }

    protected function setRequestData($data = '')
    {
        $this->requestData = !empty($data) ?: array(
            "list_name"     => $this->fields['listName'],
            "subscriptions" => array(
                array(
                    "first_name"   => $this->fields['firstName'],
                    "last_name"    => $this->fields['lastName'],
                    "email"        => $this->fields['email'],
                    "aim"          => $this->fields['aim'],
                    "mobile"       => array("number" => $this->fields['mobileNumber'], "country_code" => $this->fields['mobileCountryCode']),
                    "landline"     => array("number" => $this->fields['landlineNumber'], "country_code" => $this->fields['landlineCountryCode']),
                    "voice_device" => $this->fields['voiceDevice'],
                    "customdata"   => $this->fields['customData'],
                ),
            ),
        );        

        $this->requestData = Helper::cleanArray($this->requestData);

        return $this;
    }

}
