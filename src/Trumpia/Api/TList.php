<?php

/*
 * This file is part of the Trumpia library.
 *
 * (c) Guillaume Bourdages <gbourdages@graphem.ca>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Trumpia\Api;

/**
 * @author Guillaume Bourdages <gbourdages@graphem.ca>
 */

use Trumpia\Helper\Helper;
use Trumpia\Helper\Report as ReportHelper;


class TList extends AbstractApi
{

    /**
     * Service of the class
     *
     * @var string
     */

    protected $service = "list";

    protected $fields = array(
        'listName'    => '',
        'displayName' => '',
        'frequency'   => '',
        'description' => '',
    );

    // Important note on update() method, seems like trumpia do not like it if you pass the same list name when
    // updating it, so pass a different one of pass it as null.

    protected function setRequestData($data = '')
    {

        $this->requestData = array(
            "list_name"    => $this->fields['listName'],
            "display_name" => $this->fields['displayName'],
            "frequency"    => $this->fields['frequency'],
            "description"  => $this->fields['description']
        );

        $this->requestData = Helper::cleanArray($this->requestData);

        return $this;
    }

}
