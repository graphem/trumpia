<?php

/*
 * This file is part of the Trumpia library.
 *
 * (c) Guillaume Bourdages <gbourdages@graphem.ca>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Trumpia\Api;

/**
 * @author Guillaume Bourdages <gbourdages@graphem.ca>
 */

class Report extends AbstractApi{

    /**
     * Service of the class
     *
     * @var string
     */

    protected $service = "report";

    /**
     * Get report with delay
     *
     * @param  
     * @return 
     */

    public function fetchWithDelay($id)        
    {
        sleep(0.5);
        
        $return = $this->fetch($id);

        return $return;
    }

}