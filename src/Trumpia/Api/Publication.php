<?php

/*
 * This file is part of the Trumpia library.
 *
 * (c) Guillaume Bourdages <gbourdages@graphem.ca>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Trumpia\Api;

/**
 * @author Guillaume Bourdages <gbourdages@graphem.ca>
 */

use Trumpia\Helper\Helper;
use Trumpia\Helper\Report as ReportHelper;


class Publication extends AbstractApi
{

    /**
     * Service of the class
     *
     * @var string
     */

    protected $service = "publication";

    protected $fields = array(
        'description'    => '',
        'tweet' => '',
        'twitterAccountNames'   => '',
        'facebookPost' => '',
        'facebookAccountNames' => '',
        'facebookPravicy' => '',
        'facebookAttachmentType' => '',
        'facebookAttachmentUrl' => '',
        'facebookPagePost' => '',
        'facebookPageAccountNames' => '',
        'facebookPageAttachmentType' => '',
        'facebookPageAttachmentUrl' => '',
        'postDate' => ''
    );


    protected function setRequestData($data = '')
    {

        $this->requestData = array(
            "description" => $this->fields["description"],
            "twitter" 	=> array(
            	"tweet" => $this->fields["tweet"],
            	"account_names" => $this->fields["twitterAccountNames"]
            ),
            "facebook" => array(
            	"post" => $this->fields["facebookPost"],
            	"account_names" => $this->fields["facebookAccountNames"],
            	"privacy" => $this->fields["facebookPravicy"],
            	"attachment" => array(
            		"type" => $this->fields["facebookAttachmentType"],
            		"url" => $this->fields["facebookAttachmentUrl"]
            	)
            ),
            "facebook_page" => array(
            	"post" => $this->fields["facebookPagePost"],
            	"account_names" => $this->fields["facebookPageAccountNames"],
            	"attachment" => array(
            		"type" => $this->fields["facebookPageAttachmentType"],
            		"url" => $this->fields["facebookPageAttachmentUrl"]
            	)
            ),
            "post_date" => $this->fields["postDate"]
        );

        $this->requestData = Helper::cleanArray($this->requestData);

        return $this;
    }

}
