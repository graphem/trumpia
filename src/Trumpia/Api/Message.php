<?php

/*
 * This file is part of the Trumpia library.
 *
 * (c) Guillaume Bourdages <gbourdages@graphem.ca>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Trumpia\Api;

/**
 * @author Guillaume Bourdages <gbourdages@graphem.ca>
 */

use Trumpia\Helper\Helper;
use Trumpia\Helper\Report as ReportHelper;

class Message extends AbstractApi{

    // Available methods supported by Trumpia: put, get by id

    protected $fields = array(
        'description'         => '',
        'emailSubject'        => '',
        'emailBody'           => '',
        'emailReplyTo'        => '',
        'smsSubject'          => '',
        'smsBody'             => '',
        'imMessage'           => '',
        'mmsSubject'          => '',
        'mmsMessage'          => '',
        'mmsResource'         => '',
        'sendDate'            => '',
        'recipientsType'      => '',        // list or subscription
        'recipientsValue'     => '',
        'types'               => '',        // variable for setRequestData
        'org_name_id'         => ''         
    );

    protected $service = 'message';

    protected function setRequestData($types = array()){
        
        $this->requestData = array(
            "description" => $this->fields['description'],
            
            "recipients" => array(
                "type" => $this->fields['recipientsType'],
                "value" => $this->fields['recipientsValue']
            ),
            "send_date" => $this->fields['sendDate'],
            "org_name_id" => $this->fields['org_name_id'],

            // See if we can use package Carbon to let user use format like "tomorrow"
            //"YYYY-MM-DD hh:mm:ss" mm/dd/yy 
        );

        foreach ($types as $type) {
            
            switch ($type) {

                case 'email':
                    
                    $this->requestData['email'] = array(
                        "subject" => $this->fields['emailSubject'],
                        "body" => $this->fields['emailBody'],
                        "reply_to" => $this->fields['emailReplyTo']
                    );                   
                    break;
                
                case 'sms':

                    $subject = preg_replace("![^a-z0-9@\!\'\"#$%&()*+,-.?\/:;<=>]+!i", " ", $this->fields['smsSubject']);
                    $body = preg_replace("![^a-z0-9@\!\'\"#$%&()*+,-.?\/:;<=>]+!i", " ", $this->fields['smsBody']);

                    $this->requestData['sms'] = array(
                        "subject" => $subject,
                        "message" => $body,
                    );                    
                    break;

                case 'im':
                    
                    $this->requestData['im'] = array(
                        "message" => $this->fields['imMessage']
                    );
                    break;

                case 'mms':
                    
                    $this->requestData['mms'] = array(
                        "subject" => $this->fields['mmsSubject'],
                        "message" => $this->fields['mmsMessage'],
                        "resource" => $this->fields['mmsResource']
                    );
                    break;

                default:
                    # code...
                    break;
            }
        }

        $this->requestData = Helper::cleanArray($this->requestData);

        return $this;

    }

    /**
     * Determine gate way through types array
     */

    public function createMessages () {

        if  (empty($this->fields['types'])) {

            throw new \Exception('No types of messages were specified');
            exit;
        }

        $this->setRequestData($this->fields['types']);

        $this->return = $this->api->put('/' . $this->service, $this->getRequestData());

        return $this->getResults();
    }

}