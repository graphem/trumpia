<?php

/*
 * This file is part of the Trumpia library.
 *
 * (c) Guillaume Bourdages <gbourdages@graphem.ca>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Trumpia\Api;

/**
 * @author Guillaume Bourdages <gbourdages@graphem.ca>
 */

use Trumpia\Helper\Helper;
use Trumpia\Helper\Report as ReportHelper;


class CustomData extends AbstractApi
{

    /**
     * Service of the class
     *
     * @var string
     */

    protected $service = "customdata";

    protected $fields = array(
        'name'    => '',
        'inputType' => '',
        'required'   => '',
        'options' => ''
    );


    protected function setRequestData($data = '')
    {

        $this->requestData = array(
            "name"    => $this->fields['name'],
            "input_type" => $this->fields['inputType'],
            "required"    => $this->fields['required'],
            "options"  => $this->fields['options']
        );

        $this->requestData = Helper::cleanArray($this->requestData);

        return $this;
    }

}
