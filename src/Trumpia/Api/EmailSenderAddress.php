<?php

/*
 * This file is part of the Trumpia library.
 *
 * (c) Guillaume Bourdages <gbourdages@graphem.ca>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Trumpia\Api;

/**
 * @author Guillaume Bourdages <gbourdages@graphem.ca>
 */

use Trumpia\Helper\Helper;
use Trumpia\Helper\Report as ReportHelper;


class EmailSenderAddress extends AbstractApi
{

    /**
     * Service of the class
     *
     * @var string
     */

    protected $service = "email/sender";

    protected $fields = array(
       
        'address' => ''
    );


    protected function setRequestData($data = '')
    {

        $this->requestData = array(
            
            "address"  => $this->fields['address']
        );

        $this->requestData = Helper::cleanArray($this->requestData);

        return $this;
    }

}
