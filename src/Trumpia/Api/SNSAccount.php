<?php

/*
 * This file is part of the Trumpia library.
 *
 * (c) Guillaume Bourdages <gbourdages@graphem.ca>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Trumpia\Api;

/**
 * @author Guillaume Bourdages <gbourdages@graphem.ca>
 */

use Trumpia\Helper\Helper;
use Trumpia\Helper\Report as ReportHelper;


class SNSAccount extends AbstractApi
{

    /**
     * Service of the class
     *
     * @var string
     */

    protected $service = "account/sns";

    protected $fields = array(
        'service' => ''
    );


    protected function setRequestData($data = '')
    {

        $this->requestData = array(
            "service" => $this->fields['service']
        );

        $this->requestData = Helper::cleanArray($this->requestData);

        return $this;
    }

}
